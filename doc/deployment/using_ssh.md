## Using SSH keys in builds

> Note that the following assumes the use of docker images for builds.

If you are not in a position where you can use Dpl for deployments (eg. destinations with
APIs), then you will want to use an SSH connection for rsync or the like. This is
acheivable with Gitlab CI, but you need to pay attention to certain details when
setting up the connection.

### Getting your keys into the build environment

In order for the build environment to be able to make an SSH connection it will need
a pre-generated private key file. You will have to generate this on your own machine
and inject it into the container manually. Note that you will have to leave the
password blank when generating the key, as the build environment will not be able to
unlock the key as that would require an interactive prompt. As always, the public
key file will need to be copied to any destinations

There are two ways to inject:

1. Using a volume bind-mounted to the gitlab-runner host
2. Using private variables

#### Using volumes

With this method, we need to modify the configuration file of each gitlab-runner
instance to attach an additional volume from the filesystem, and make sure the
private key file is available there. The target would be `/root/.ssh/`.

For more information on how to add a custom volume to a gitlab-runner instance,
see [the documentation](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/configuration/advanced-configuration.md#the-runnersdocker-section).

#### Using private variables

This simply involves copying the private key file contents into a private variable
on a per project basis. In Gitlab CI, go to "Variables" in the sidebar and enter a
name like `SSH_PRIVATE_KEY` into the key field and the file contents into the value
field. Save changes and you should be ready for the next step.

You're build environment will need to write the private key file somewhere and
properly set permissions on it. An example of how this looks follows.

	script:
	  # Add private key so we can connect to destination server
      - echo "$SSH_PRIVATE_KEY" > id_rsa
      - chmod 0600 id_rsa
      # Deploy
      - rsync -prvzcSle 'ssh -p 22 -o StrictHostKeyChecking=no -i id_rsa' ./ user@server-ip:/path/to/destination

Note a few important details from the above:

- When using `echo` to write the file, you *must* use quotes around the private variable,
  otherwise the new lines will be interpreted as spaces and your key will be denied.
- You *must* change permissions on the file to 0600 or ssh will complain.
- You *must* turn off StrictHostKeyChecking in the build environment's ssh config. This can
  be done, as above, by passing a parameter to the ssh command. You may also update your
  build container to include that configuration change in the `/etc/sshd_config` file, or
  write to it in the deploy script. Failure to turn off this feature will cause the build
  to fail, as it would otherwise require you to interactively accept the new server's
  host key. Since every build is a new container instance, the destination server will be
  absent in the `~/.ssh/known_hosts` file each time. Alternatively, you can ensure that
  the correct host key is written to that file.
